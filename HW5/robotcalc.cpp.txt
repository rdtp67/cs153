//Programer: Ryan Tinsley
//Section: 1A

#include <string>
#include <cstdlib>
#include <iostream>
#include "arraystack.h"
#include "abstractstack.h"
using namespace std;

int main()
{
 int data;
 char opp;
 string s1;
 ArrayStack<int> stack;

 do
 {
  getline(cin, s1, '\n');
  if( isdigit(s1[0]))
  {
   data = atoi( s1.c_str() );
   stack.push( data );
  }
  else
  {
   opp = s1[0];
  if(opp != '@')
  {
   switch (opp)
   {
    case '+':
      stack.calc_add();
      break;
    case '-':
      stack.calc_sub();
      break;
    case '*':
      stack.calc_mult();
      break;
    case '/':
      stack.calc_div();
      break;
    case '%':
      stack.calc_mod();
      break;
 case '!':
      stack.calc_neg();
      break;
    case 's':
      stack.calc_sum();
      break;
    case 'p':
      stack.calc_prod();
      break;
    case '#':
      stack.print();
      break;
    case '$':
      stack.clear();
      break;
    default:
      cout << "Error ~Opperation Unknown~ " << endl;

   };
  }
  }


 }while(opp != '@');

 return 0;
}
