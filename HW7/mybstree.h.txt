//Programer: Ryan Tinsley
//Section: 1A

#ifndef MYBSTREE_H
#define MYBSTREE_H

#include <iostream>
#include <string>
using namespace std;

template <typename T>
struct BSTreeNode
{
 T m_data;
 BSTreeNode* m_right;
 BSTreeNode* m_left;
 BSTreeNode()
 {
  m_data = NULL;
  m_left = NULL;
  m_right = NULL;
 }
 BSTreeNode(T t)
 {
  m_data = t;
  m_left = NULL;
  m_right = NULL;
 }
   ~BSTreeNode()
 {
  delete m_left;
  m_left = NULL;
  delete m_right;
  m_right = NULL;
 }
};


template < typename T >
class MyBSTree
{
 private:

 public:

  BSTreeNode<T>* m_root;
  MyBSTree()
 {
   m_root = NULL;
  }
  bool isEmpty()
  {
   if(m_root == NULL)
    return true;
    return false;
  }
  int size();//done
  int size(BSTreeNode<T>* root);//done
  int height();//done
  int height(BSTreeNode<T>* root);//done
  const T& findMax();//done
  const T& findMin();//done
  int contains(const T& x);//done
  int contains(const T& x, BSTreeNode<T>* root);//done
  void clear();//done
  void insert(const T& x);//done
  void insert(const T& x, BSTreeNode<T>* &root);//done
  void remove(const T& x);
  void remove(const T& x, BSTreeNode<T>* root);
  void printPreOrder();//done
  void printPreOrder(BSTreeNode<T>* root);//done
  void printPostOrder();//done
  void printPostOrder(BSTreeNode<T>* root);//done
  void print();//done
  void print(int pad, BSTreeNode<T>* p);//done
  ~MyBSTree()//done
  {
   delete m_root;
  }
  MyBSTree(const MyBSTree &rhs);//done
  void copyhelper(BSTreeNode<T>*& copy, const BSTreeNode<T>* p);//done
  const void operator= (const MyBSTree &rhs);//done
};

template <typename T>
MyBSTree<T>:: MyBSTree(const MyBSTree &rhs)
{
 copyhelper(m_root,rhs.m_root);
}

template <typename T>
void MyBSTree<T>:: copyhelper(BSTreeNode<T>*& copy,const BSTreeNode<T>* p)
{
 if(p==NULL)
 {
 copy = NULL;
 }
 else
 {
  copy = new BSTreeNode<T>;
  copy->m_data = p->m_data;
  copyhelper(copy->m_left, p->m_left);
  copyhelper(copy->m_right, p->m_right);
 }
}


template <typename T>
const void MyBSTree<T>:: operator=(const MyBSTree &rhs)
{
 if(&rhs == this)
  return;
 delete m_root;
 m_root = NULL;
 copyhelper(m_root, rhs.m_root);
}


template <typename T>
void MyBSTree<T>:: insert(const T& x)
{
 insert(x, m_root);
}

template <typename T>
void MyBSTree<T>:: insert(const T& x, BSTreeNode<T>* &root)
{
 if(root == NULL)
 {
  root = new BSTreeNode<T>(x);
  return;
 }
 if(x < root->m_data)
 {
  insert(x, root->m_left);
 }
 else
 {
  insert(x, root->m_right);
 }

}
template <typename T>
void MyBSTree<T>:: print()
{
 print(0,m_root);
}

template <typename T>
void MyBSTree<T>:: print(int pad, BSTreeNode<T>* p)
{
 if(p==NULL)
 {
  cout<<endl;
 }
 else
 {
  print(pad+4, p->m_right);
  for(int i=0; i<pad; i++)
  {
   cout << "    ";
  }
  cout << p->m_data;
  print(pad+4, p->m_left);
 }
}

template <typename T>
void MyBSTree<T>:: printPreOrder()
{
 if(m_root != NULL)
  printPreOrder(m_root);
 else
  cout << "Error ~Bianary Search Tree Is Empty~" << endl;
}

template <typename T>
void MyBSTree<T>:: printPreOrder(BSTreeNode<T>* root)
{
 if(root!=NULL)
 {
  cout<<root->m_data<< endl;
  printPreOrder(root->m_left);
  printPreOrder(root->m_right);
 }
}

template <typename T>
void MyBSTree<T>:: printPostOrder()
{

 if(m_root != NULL)
  printPostOrder(m_root);
 else
  cout << "Error ~Bianary Search Tree Is Empty~" <<endl;
}

template <typename T>
void MyBSTree<T>:: printPostOrder(BSTreeNode<T>* root)
{
 if(root != NULL)
 {
  printPostOrder(root->m_left);
  printPostOrder(root->m_right);
  cout << root->m_data<<endl;
 }
}

template <typename T>
int MyBSTree<T>::size()
{
 return size(m_root);
}

template <typename T>
int MyBSTree<T>:: size(BSTreeNode<T>* root)
{
 if(root==NULL)
 {
  return (0);
 }
 else
 {
  return(size(root->m_left) + 1 + size(root->m_right));
 }
}

template <typename T>
int MyBSTree<T>:: height()
{
 return height(m_root);
}

template <typename T>
int MyBSTree<T>:: height(BSTreeNode<T>* root)
{
 if(root == NULL)
  return (0);
 else
 {
  int lheight = height(root->m_left);
  int rheight = height(root->m_right);

  if(lheight > rheight)
   return (lheight+1);
  else
   return (rheight+1);
 }

}

template <typename T>
const T& MyBSTree<T>:: findMax()
{
 BSTreeNode<T>* cur = m_root;
 while(cur->m_right!=NULL)
 {
  cur = cur->m_right;
 }
 return (cur->m_data);
}

template <typename T>
const T& MyBSTree<T>:: findMin()
{
 BSTreeNode<T>* cur = m_root;
 while(cur->m_left != NULL)
 {
  cur = cur->m_left;
 }
 return (cur->m_data);
}

template <typename T>
int MyBSTree<T>:: contains(const T& x)
{
 return contains(x,m_root);
}

template <typename T>
int MyBSTree<T>:: contains(const T& x, BSTreeNode<T>* root)
{
 if(root == NULL)
 {
  cout << "Error ~Bianary Search Tree Does Not Contain Element~" << endl;
  return 0;
 }
 else
 {
  if(root->m_data == x)
   return height(root);
  else
  {
   if(root->m_data > x)
    return contains(x,root->m_left);
   else
    return contains(x,root->m_right);
  }
 }
}

template <typename T>
void MyBSTree<T>:: clear()
{
 delete m_root;
 m_root = NULL;
}
/*
template <typename T>
void MyBSTree<T>::remove(const T& x)
{
 remove(x,m_root);
}

template <typename T>
void MyBSTree<T>::remove(const T& x, BSTreeNode<T> root)
{
 if(x < root->m_data)
 {
  remove(x, root->m_left)
 }
 else if(x>root->m_data)
 {
  remove(x, root->m_right);
 }
 else
 {
  if(root->m_left != NULL && root->m_right != NULL)
  {
   root->m_data = root_
  }
 }
}
*/
#endif
